module.exports = function(grunt) {

  grunt.initConfig({

    concat: {
      blocks_script: {
        src:'blocks/*/*.js',
        dest: 'blocks/blocks.js'
      },

      blocks_style: {
        src: 'blocks/*/*.scss',
        dest: 'blocks/blocks.scss'
      },

      blocks_swig: {
        src: 'blocks/*/*.swig',
        dest: 'blocks/blocks.swig'
      },

      pages_script: {
        src:'pages/*/*.js',
        dest: 'pages/pages.js'
      },

      pages_style: {
        src: 'pages/*/*.scss',
        dest: 'pages/pages.scss'
      },

      // pages_view: {
      //   src: 'pages/*/*.view',
      //   dest: 'pages/pages.view'
      // },

      base_scss: {
        options: {
          banner: '@charset "utf-8"; @import "compass"; \n\n',
          // banner: '@charset "utf-8"; @import "compass";\n\n $screen-phone: 480px; \n $screen-tablet: 768px; \n $screen-desktop: 1020px; \n $screen-lg-desktop: 1200px; \n $screen-xs-max: ($screen-tablet - 1); \n $screen-sm-max: ($screen-desktop - 1); \n $screen-md-max: ($screen-lg-desktop - 1);\n\n',
        },
        src: 'scss/base/*.*',
        dest: 'scss/base.scss'
      },

      js_plugins: {
        src: 'js/plugins/*.js',
        dest: 'web/js/plugins.js'
      },

      style_scss: {
        options: {
          banner: '@charset "utf-8"; @import "compass"; \n\n',
          // banner: '@charset "utf-8"; @import "compass";\n\n $screen-phone: 480px; \n $screen-tablet: 768px; \n $screen-desktop: 1020px; \n $screen-lg-desktop: 1200px; \n $screen-xs-max: ($screen-tablet - 1); \n $screen-sm-max: ($screen-desktop - 1); \n $screen-md-max: ($screen-lg-desktop - 1);\n\n',
        },
        src: ['blocks/blocks.scss', 'pages/pages.scss'],
        dest: 'scss/style.scss'
      },

      script_js: {
        src: ['blocks/blocks.js', 'pages/pages.js'],
        dest: 'web/js/script.js'
      },
    },

    compass: {
      base: {
        options: {
          sassDir: 'scss',
          specify: 'scss/base.scss',
          cssDir: 'web/css',
          environment: 'production',
          outputStyle: 'compressed',
          noLineComments: true
        }
      },

      style: {
        options: {
          sassDir: 'scss',
          specify: 'scss/style.scss',
          cssDir: 'web/css',
          environment: 'production',
          outputStyle: 'compact',
          noLineComments: true
        }
      }
    },

    swig: {
      view: {
        src: 'blocks/*/*.view',
        dest: 'web',
        // url: '../',
        generateSitemap: false,
        generateRobotstxt: false
      },

      pages: {
        src: 'pages/*/*.view',
        dest: 'web',
        // url: '../',
        generateSitemap: false,
        generateRobotstxt: false
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 3 version', 'ie 9']
      },
      style: {
        src: 'web/css/style.css',
        dest: 'web/css/style.css'
      },
    },

    watch: {
      blocks_script: {
        files: '<%= concat.blocks_script.src %>',
        tasks: [
          'concat:blocks_script',
          'concat:script_js'
        ],
        options: {
          livereload: true,
        },
      },
      blocks_style: {
        files: '<%= concat.blocks_style.src %>',
        tasks: [
          'concat:blocks_style',
          'concat:style_scss',
          'compass:style',
          'autoprefixer'
        ],
        options: {
          livereload: true,
        },
      },
      blocks_swig: {
        files: '<%= concat.blocks_swig.src %>',
        tasks: [
          'concat:blocks_swig'
        ],
        options: {
          livereload: true,
        },
      },
      pages_script: {
        files: '<%= concat.pages_script.src %>',
        tasks: [
          'concat:pages_script',
          'concat:script_js'
        ],
        options: {
          livereload: true,
        },
      },
      pages_style: {
        files: '<%= concat.pages_style.src %>',
        tasks: [
          'concat:pages_style',
          'concat:style_scss',
          'compass:style',
          'autoprefixer'
        ],
        options: {
          livereload: true,
        },
      },
      base_scss: {
        files: '<%= concat.base_scss.src %>',
        tasks: [
          'concat:base_scss',
          'compass:base'
        ],
        options: {
          livereload: true,
        },
      },
      blocks_style: {
        files: '<%= concat.blocks_style.src %>',
        tasks: [
          'concat:blocks_style',
          'concat:style_scss',
          'compass:style',
          'autoprefixer'
        ],
        options: {
          livereload: true,
        },
      },
      blocks_layout: {
        files: 'blocks/layout.swig',
        tasks: 'swig',
        options: {
          livereload: true,
        },
      },
      swig_views: {
        files: ['blocks/*/*.view', 'pages/*/*.view'],
        tasks: 'swig',
        options: {
          livereload: true,
        },
      },
      swig_blocks: {
        files: ['blocks/*/*.swig'],
        tasks: ['concat:blocks_swig',
                'swig'],
        options: {
          livereload: true,
        },
      }
    },
    connect: {
      server: {
        options: {
          port: 9001,
          base: 'web',
          livereload: 'true',
          hostname: '*'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  // grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-swig'); 
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-contrib-connect');
  // grunt.loadNpmTasks('grunt-contrib-copy');
  // grunt.registerTask('default', ['concat', 'uglify', 'compass', 'swig', 'watch']);
  grunt.registerTask('default', ['concat', 'connect', 'compass', 'autoprefixer', 'swig', 'watch' ]);

};
