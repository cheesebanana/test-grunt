

$(document).ready(function() {
  $("#menu-toggle").on("click", function(){
    $("#menubar").toggleClass('active');
    $('.nav-container ul li').each(function(i){
      var t = $(this);
      setTimeout(function(){ 
        setTimeout(function(){ t.toggleClass('animation'); }, (i+1) * 30); 
      }, 200);
      
    });
  });
});
$(document).ready(function(){
  $(function(){
    $('#tabscont').mixItUp();  
  });
});
var imagesFromServer = [
    "images/1.jpg",
    "http://placehold.it/800x800/3453",
    "http://placehold.it/800x800/343",
    "http://placehold.it/800x800/142",
    "http://placehold.it/800x800/4434",
    "http://placehold.it/800x800/1436"
    ];
var images = imagesFromServer.slice();

$(document).ready(function($) {
  $(".vw").fitText(1.8);
  $(".square-downl .capt").fitText(0.65);
  $(images).preload();
  images.sort(function() {
    return Math.random() - 0.5;
  });

  // s(Math.floor(Math.random() * (9 - 0 + 1)) + 0);
  // changeRandom($(".insta img, #aloneim"), images, imagesFromServer);

  $(".insta img, #aloneim").on('mouseenter', function(event) {
    var that = $(this);
    images = changeImage(that, images, imagesFromServer);
    
  });
});

$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
    console.log("preloaded", this.length)
}

function changeImage(inst, images, imagesFromServer) {
  s(inst.src);
  if (inst.src === images[0])
    images.sort(function() {
      return Math.random() - 0.5;
    });
  $(inst).attr('src', images[0]);
  images.splice(0,1);
  
  if (images.length === 0) {
    s(1);
    s(images);
    images = imagesFromServer.slice();
    s(images);
    images.sort(function() {
      return Math.random() - 0.5;
    });
    
  };
  return images;
}

function changeRandom(arr, images, imagesFromServer) {
  var max = arr.length;
  var min = 0;
  var rand = Math.floor(Math.random() * (max - min + 1)) + min;
  s(arr[rand].src);
  images = changeImage(arr[rand], images, imagesFromServer);
  // changeRandom(arr, images, imagesFromServer);
}
$(document).ready(function() {
    if ($('#map').length > 0) {
        initGoogleMap(50.45945921558483, 30.514845550060272);
    }
});

function initGoogleMap(xcoord,ycoord) {
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "http://maps.google.com/maps/api/js?v=3&sensor=false&callback=gmap_draw";

    window.gmap_draw = function() {
        latlng = new google.maps.LatLng(xcoord,ycoord);

        var myOptions = {
            zoom: 15,
            center: latlng,
            mapTypeControl: false,
            overviewMapControl: false,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var styles = [
            {
                featureType: "all",
                elementType: "all",
                stylers: [
                    // { hue: "#fff200" },
                    {saturation: "-100"},
                    {lightness: '20'}
                ]
            }
        ];

        map = new google.maps.Map(document.getElementById("map"), myOptions);

        mapType = new google.maps.StyledMapType(styles);
        map.mapTypes.set('tehgrayz', mapType);
        map.setMapTypeId('tehgrayz');
        placeMarker();

        /* coords to console with click */
        google.maps.event.addListener(map, 'click', function(event) {
            console.log(event.latLng);
        });

    };

    $("head").append(s);
}

/* Отображает маркер ЧЗБ на карте */
function placeMarker() {

    var marker = new google.maps.Marker({
        raiseOnDrag: false,
        map: map,
        position: latlng,
        title: "CHEESEBANANA"
    });

}

